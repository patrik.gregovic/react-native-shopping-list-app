import React from 'react';//import react
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';// import react-native components
import Icon from '@expo/vector-icons/FontAwesome';
//create component
const ListItem = ({ item, deleteItem }) => {//pass value for title prop
    return (
        <TouchableOpacity style={styles.listItem}>
            <View style={styles.listItemView}>
                <Text style={styles.listItemText}>
                    {item.text}
                </Text>
                <Icon
                    name="trash" size={20} color="firebrick"
                    onPress={() => deleteItem(item.id)}
                />
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    listItem: {
        padding: 23,
        backgroundColor: '#f8f8f8',
        borderBottomWidth: 1,
        borderColor: '#eee',
    },
    listItemView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    listItemText: {
        fontSize: 18,
    },
});

export default ListItem;