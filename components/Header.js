import React from 'react';//import react
import { View, Text, StyleSheet } from 'react-native';// import react-native components

//create component
const Header = ({ title }) => {//pass value for title prop
    return (
        <View style={styles.header}>
            <Text style={styles.text}>{title}</Text>
        </View>
    );
};

// default values for props
// when props are beign used but no value was passed
Header.defaultProps = {
    title: 'Shopping List',//default value for title
};

const styles = StyleSheet.create({
    header: {
        height: 60,
        padding: 15,
        backgroundColor: 'darkslateblue',
    },
    text: {
        color: '#fff',
        fontSize: 23,
        textAlign: 'center',
    },
});

export default Header;