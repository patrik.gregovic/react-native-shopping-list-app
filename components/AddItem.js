import React, { useState } from 'react';//import react
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';// import react-native components
import Icon from '@expo/vector-icons/FontAwesome';

//create component
const AddItem = ({ addItem }) => {//pass value for title prop
    const [text, setText] = useState('');

    var textInput;

    const onChange = function (textValue) { setText(textValue) };

    function clearInputField() { textInput.clear(); setText(''); }

    return (
        <View>
            <View style={styles.inputContainer}>
                <TextInput placeholder="Add Item..." style={styles.input} onChangeText={onChange} ref={input => { textInput = input }} />
                <TouchableOpacity onPress={() => { clearInputField() }} style={styles.clearButton}>
                    <Icon
                        name="remove" size={20} color="darkslateblue"
                    />
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.btn} onPress={function () { clearInputField(); addItem(text); }}>
                <Text style={styles.btnText}>
                    <Icon name="plus" size={20} /> Add Item
                </Text>
            </TouchableOpacity>
        </View>
    );
};

// default values for props
// when props are beign used but no value was passed
AddItem.defaultProps = {
    title: 'Shopping List',//default value for title
};

const styles = StyleSheet.create({
    input: {
        height: 60,
        marginLeft: 23,
        fontSize: 16,
    },
    btn: {
        backgroundColor: '#d2bad8',
        padding: 10,
        margin: 23,
    },
    btnText: {
        color: 'darkslateblue',
        fontSize: 20,
        textAlign: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    clearButton: {
        paddingTop: 20,
        marginRight: 23,
    },
});

export default AddItem;