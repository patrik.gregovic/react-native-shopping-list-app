import React, { useState } from 'react';//import react
import { View, Text, StyleSheet, FlatList, Alert } from 'react-native';// import react-native components
import 'react-native-get-random-values';
import { v4 as uuid } from 'uuid';

import Header from './components/Header';
import ListItem from './components/ListItem';
import AddItem from './components/AddItem';

//create component
const App = () => {
  const [items, setItems] = useState([
    { id: uuid(), text: 'Milk' },
    { id: uuid(), text: 'Eggs' },
    { id: uuid(), text: 'Bread' },
    { id: uuid(), text: 'Juice' },
  ]);

  const deleteItem = function (id) {
    setItems(
      function (prevItems) {
        return prevItems.filter(item => item.id != id);
      }
    );
  };

  const addItem = function (text) {
    if (!text) {
      Alert.alert('No item entered', 'Please enter an item.', [{ text: 'Ok' },], { cancelable: true });
    }
    else {
      setItems(
        function (prevItems) {
          return [{ id: uuid(), text }, ...prevItems]; //insert new item and then all previous items
        }
      );
    }
  };

  return (
    <View style={styles.container}>
      <Header />
      <AddItem addItem={addItem} />
      <FlatList
        data={items}
        renderItem={({ item }) => (<ListItem item={item} deleteItem={deleteItem} />)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
  },
});

export default App;